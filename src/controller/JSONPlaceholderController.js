const axios = require("axios");

module.exports = class
JSONPlaceholderController {

    static async getUsers(req, res) {
        try {
          const response = await axios.get("https://jsonplaceholder.typicode.com/users");
          res.status(200).json(response.data); // Retorne os dados diretamente
        } catch (error) {
          console.error(error);
          res.status(500).json({ error: "Falha ao encontrar usuários" });
        }
      }
      
      static async getUsersWebsiteIO(req, res){
    
        try{
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            const users = response.data.filter((user)=> user.website.endsWith(".io"))
            const Banana = users.length
            res.status(200).json({ message:"AQUI ESTÃO OS USERS COM DOMINIO IO. Quantos bananas tem esse dominio:",Banana,
 users,
        });
           
        }
        catch{
            console.error(error);
          res.status(500).json({ error: "Falha ao encontrar usuários" });
        }
      }

      static async getUsersWebsiteNET(req, res){
        try{
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            const users = response.data.filter((user)=> user.website.endsWith(".net"))
            const Banana = users.length
            res.status(200).json({ message:"AQUI ESTÃO OS USERS COM DOMINIO IO. Quantos bananas tem esse dominio:",Banana,
 users,});
        }
        catch{
            console.error(error);
          res.status(500).json({ error: "Falha ao encontrar usuários" });
        }
      }

      static async getUsersWebsiteCOM(req, res){
        try{
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            const users = response.data.filter((user)=> user.website.endsWith(".com"))
            const Banana = users.length
            res.status(200).json({ message:"AQUI ESTÃO OS USERS COM DOMINIO IO. Quantos bananas tem esse dominio:",Banana,
 users,});
        }
        catch{
            console.error(error);
          res.status(500).json({ error: "Falha ao encontrar usuários" });
        }
      }

      // req.query

      static async filtroWebsite(req, res){
        const { dominio } = req.query
        if(!dominio) {
          res.status(400).json({ message:`Informe um domínio válido`});
        }
        try{
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            const users = response.data.filter((user)=> user.website && user.website.endsWith(dominio))

            res.status(200).json({ message:`Existem ${users.length} usuários com esse domínio`});
        }
      
        catch{
            console.error(error);
          res.status(500).json({ error: "Falha ao encontrar Nutricionista" });
        }
      }




};