const express = require('express')
const cors = require ('cors')
const app = express()

class AppController {
    constructor() {
      this.express = express();
      this.middlewares();
      this.routes();
    }

    middlewares() {
      this.express.use(express.json());
      this.express.use(cors());
    }

    routes() {
      const apiRoutes= require('./routes/apiRoutes')
      this.express.use('/santucci/', apiRoutes);
    this.express.get("/health/", (_, res) => {
      res.send({ status: "APLICAÇÃO NO AR" });
    });
  }
}


  module.exports = new AppController().express;