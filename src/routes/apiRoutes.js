const router = require('express').Router()
const algumaController = require('../controller/teacherController')
const alunoController = require('../controller/alunoController')
const JSONPlaceholderController = require('../controller/JSONPlaceholderController')


router.get('/alguma/', algumaController.getAlgumaCoisa);
router.post('/cadastroaluno/', alunoController.postAluno );
router.put('/atualizaraluno/', alunoController.editarAluno);
router.delete('/deletaraluno/:id', alunoController.deleteAluno);

router.get("/external/", JSONPlaceholderController.getUsers)
router.get("/external/io", JSONPlaceholderController.getUsersWebsiteIO)
router.get("/external/com", JSONPlaceholderController.getUsersWebsiteCOM)
router.get("/external/net", JSONPlaceholderController.getUsersWebsiteNET)
router.get("/external/filter", JSONPlaceholderController.filtroWebsite)



module.exports = router